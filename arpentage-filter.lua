--
-- arpentage: divide a book in multiple parts of equal size
--
-- Copyright © 2020 Lunar, John MacFarlane
-- This is free software under the “Expat” or “MIT” license.
--
-- This is a lua filter for Pandoc >= 2.0.
-- See https://pandoc.org/lua-filters.html for more details.
--
-- Based on the wordcount.lua script by John MacFarlane.
--

-- Constants
local WORDS_PER_MINUTE = 180

-- Parameters through environment variables
local parts = tonumber(os.getenv("PARTS")) or nil
local verbose = tonumber(os.getenv("VERBOSE")) or 0
local current_part = tonumber(os.getenv("CURRENT_PART")) or nil
local words_per_part = tonumber(os.getenv("WORDS_PER_PART")) or nil
local append_to_title = os.getenv("APPEND_TO_TITLE")

-- “Globals”
local word_count = nil
local in_section = false

-----------------------------------------------------------------------
-- Word counting functions used in all passes
--

local function count_str(el)
  -- we don't count a word if it's entirely punctuation:
  if el.text:match("%P") then
      word_count = word_count + 1
  end
end

local function count_code(el)
  _,n = el.text:gsub("%S+","")
  word_count = word_count + n
end

local function count_code_block(el)
  _,n = el.text:gsub("%S+","")
  word_count = word_count + n
end

-----------------------------------------------------------------------
-- Pure word counting:
-- used when CURRENT_PART or WORDS_PER_PART have not been specified.
--

local wordcount = {
  Str = count_str,
  Code = count_code,
  CodeBlock = count_code_block
}

local count_words = {
  Pandoc = function(el)
    -- Reset our word counter
    word_count = 0
    -- skip metadata, just count body:
    pandoc.walk_block(pandoc.Div(el.blocks), wordcount)
    words_per_part = word_count / parts
    if verbose > 0 then
      io.stderr:write(string.format("%d words found!\n", word_count))
      io.stderr:write(string.format("So… we want %d words for each of the %d parts.\n", math.floor(words_per_part), parts))
      io.stderr:write(string.format("Using a conservative estimate of %d words per minute, this means %d minutes to read.\n", WORDS_PER_MINUTE, math.ceil(words_per_part / 180)))
    end
    print(words_per_part)
    os.exit(0)
  end
}

-----------------------------------------------------------------------
-- Find out what we want to keep and handle inlines and code blocks
--

local function cleanup(el)
  if in_section then
    return el
  else
    return {}
  end
end

local function maybe_tag_or_cleanup(el)
  part = math.min(parts, math.floor(word_count / words_per_part) + 1)
  if part == current_part then
    if not in_section then
      in_section = true
      return {pandoc.RawInline("arpentage", "start"), el}
    else
      return el
    end
  else
    if in_section then
      in_section = false
      return pandoc.RawInline("arpentage", "end")
    else
      return {}
    end
  end
end

local find_wanted_part = {
  Pandoc = function(el)
    -- Reset our word counter
    word_count = 0
    -- keep metadata as is, just work on body:
    new_div = pandoc.walk_block(pandoc.Div(el.blocks), {
      -- inlines
      Cite = cleanup,
      Code = function(el)
        count_code(el)
        return maybe_tag_or_cleanup(el)
      end,
      Emph = cleanup,
      Image = cleanup,
      LineBreak = cleanup,
      Link = cleanup,
      Math = cleanup,
      Note = cleanup,
      Quoted = cleanup,
      RawInline = function(el)
        if el.format == 'arpentage' or in_section then
          return el
        else
          return {}
        end
      end,
      SmallCaps = cleanup,
      SoftBreak = cleanup,
      Space = cleanup,
      Span = cleanup,
      Str = function(el)
        count_str(el)
        return maybe_tag_or_cleanup(el)
      end,
      Strikeout = cleanup,
      Strong = cleanup,
      Subscript = cleanup,
      Superscript = cleanup,
      Underline = cleanup,
      -- blocks
      CodeBlock = function(el)
        count_code_block(el)
        return maybe_tag_or_cleanup(el)
      end,
    })
    el.blocks = new_div.content
    return el
  end
}

-----------------------------------------------------------------------
-- Clean up the result
--

local function lookup_delimiters(el)
  seen = 'none'
  pandoc.walk_block(el, {
    RawInline = function(el)
      if el.format == 'arpentage' then
        if el.text == 'start' then
          seen = 'start'
        elseif el.text == 'end' then
          if seen == 'start' then
            seen = 'both'
          else
            seen = 'end'
          end
        end
      end
      return el
    end,
  })
  return seen
end

local function cleanup_block(el)
  seen = lookup_delimiters(el)
  if seen == 'start' then
    in_section = true
  elseif seen == 'end' then
    in_section = false
  elseif seen == 'none' then
    return cleanup(el)
  end
  return el
end

local function graft_title(m)
  if string.len(append_to_title) > 0 then
    l = pandoc.List(m.title or 'Untitled')
    l:insert(pandoc.Str(string.format(" " .. append_to_title, current_part, parts)))
  end
  return m
end

local fixup = {
  Meta = graft_title,
  Pandoc = function(el)
    in_section = false
    new_div = pandoc.walk_block(pandoc.Div(el.blocks), {
      BlockQuote = cleanup_block,
      BulletList = cleanup_block,
      CodeBlock = cleanup_block,
      DefinitionList = cleanup_block,
      Div = cleanup_block,
      Header = cleanup_block,
      HorizontalRule = cleanup_block,
      LineBlock = cleanup_block,
      Null = cleanup_block,
      OrderedList = cleanup_block,
      Para = cleanup_block,
      Plain = cleanup_block,
      RawBlock = cleanup_block,
      Table = cleanup_block,
    })
    el.blocks = new_div.content
    return el
  end
}

-----------------------------------------------------------------------
-- Find out what we need to run depending on environment variables
--

if not parts then
  error("PARTS environment variable is missing or wrong.")
end

if not words_per_part or not current_part then
  return {count_words}
else
  if not words_per_part then
    error("WORDS_PER_PART environment variable is missing or wrong.")
  end
  if not current_part then
    error("CURRENT_PART environment variable is missing or wrong.")
  end
  if current_part > parts then
    error("CURRENT_PART is too big in regards to PARTS.")
  end

  return {find_wanted_part, fixup}
end
